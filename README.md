# Intro

This is a tool for testing GPU memory swap behavior . It allocates a bunch of buffers and will cycle through them. With N buffers, the test will do N submits, each referencing one buffer.

To test swap behavior allocate more buffers than fit in the intended memory and see what happens.

# Usage:

```
./swap_test /dev/dri/renderD128 buffer_count domain flags
```

`buffer_count` is the number of 1 GiB buffers to allocate and cycle through.

`domain` is an or of
```
cpu = 1
gtt = 2
vram = 4
```

`flags` is an or of
```
uswc = 4
vram_cleared = 8
vram_contiguous = 32
vm_always_valid = 64
```

(more options found in `amdgpu_drm.h`)
