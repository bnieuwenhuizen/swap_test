/*
 * Copyright © 2020 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <fcntl.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>

#include <amdgpu.h>
#include <amdgpu_drm.h>

namespace {
	amdgpu_device_handle dev;
	amdgpu_context_handle ctx;
}

struct Buffer {
	amdgpu_bo_handle bo;
	amdgpu_va_handle va;
	uint64_t address;
	uint32_t raw_bo;
};

int create_buffer(uint64_t size, uint32_t alignment, unsigned domain, unsigned flags, Buffer *buf)
{
	amdgpu_bo_alloc_request alloc = {};
	alloc.alloc_size = size;
	alloc.phys_alignment = alignment;
	alloc.preferred_heap = domain;
	alloc.flags = flags;
	int ret = amdgpu_bo_alloc(dev, &alloc, &buf->bo);
	if (ret < 0)
		return ret;

	ret = amdgpu_va_range_alloc(dev, amdgpu_gpu_va_range_general, size, alignment, 0, &buf->address, &buf->va, 0);
	if (ret < 0)
		return ret;

	ret = amdgpu_bo_va_op(buf->bo, 0, size, buf->address,
			      AMDGPU_VM_PAGE_READABLE | AMDGPU_VM_PAGE_WRITEABLE |
			      AMDGPU_VM_PAGE_EXECUTABLE, AMDGPU_VA_OP_MAP);
	if (ret < 0)
		return ret;

	ret = amdgpu_bo_export(buf->bo, amdgpu_bo_handle_type_kms, &buf->raw_bo);

	return ret;
}


int setup_cmdbuffer(Buffer *buf) {
	int ret = create_buffer(4096, 4096, AMDGPU_GEM_DOMAIN_GTT, 0, buf);
	if (ret < 0)
		return ret;

	void *handle;
	ret = amdgpu_bo_cpu_map(buf->bo, &handle);
	if (ret < 0)
		return ret;

	uint32_t *code = (uint32_t*)handle;
	for (unsigned i = 0; i < 1024; ++i) {
		code[i] = 0xffff1000; // nop
	}

	return 0;
}

int run_test(Buffer *cmdbuffer, Buffer *data) {
	int ret;
	uint64_t seq_no;
	struct drm_amdgpu_cs_chunk chunks[2] = {};
	struct drm_amdgpu_cs_chunk_ib ib = {};
	struct drm_amdgpu_bo_list_in bo_list = {};
	struct drm_amdgpu_bo_list_entry bo_entries[2] = {};

	ib.va_start = cmdbuffer->address;
	ib.ib_bytes = 1024;
	ib.ip_type = AMDGPU_HW_IP_COMPUTE;

	bo_entries[0].bo_handle = cmdbuffer->raw_bo;
	bo_entries[0].bo_priority = 9;
	bo_entries[1].bo_handle = data->raw_bo;
	bo_entries[1].bo_priority = 8;

	bo_list.bo_number = 2;
	bo_list.bo_info_size = sizeof(bo_entries[0]);
	bo_list.bo_info_ptr = (uintptr_t)bo_entries;

	chunks[0].chunk_id = AMDGPU_CHUNK_ID_BO_HANDLES;
	chunks[0].length_dw = sizeof(bo_list) / 4;
	chunks[0].chunk_data = (uintptr_t)&bo_list;
	chunks[1].chunk_id = AMDGPU_CHUNK_ID_IB;
	chunks[1].length_dw = sizeof(ib) / 4;
	chunks[1].chunk_data = (uintptr_t)&ib;

	ret = amdgpu_cs_submit_raw2(dev, ctx, 0, 2, chunks, &seq_no);
	if (ret)
		return ret;

	bool busy;
	ret = amdgpu_bo_wait_for_idle(cmdbuffer->bo, 10000000000ull, &busy);
	if (ret)
		return ret;
	if (busy)
		return -ETIME;
	return 0;
}

int main(int argc, char * argv[])
{
	std::string node = "/dev/dri/renderD128";
	unsigned buffer_count = 1;
	unsigned domain = AMDGPU_GEM_DOMAIN_VRAM;
	unsigned flags = 0;

	if (argc >= 2)
		node = argv[1];
	if (argc >= 3)
		buffer_count = atoi(argv[2]);
	if (argc >= 4)
		domain = atoi(argv[3]);
	if (argc >= 5)
		flags = atoi(argv[4]);

	int fd = open(node.c_str(), O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "Failed to open \"%s\"\n", node.c_str());
		return 1;
	}

	uint32_t maj_ver, min_ver;
	int ret = amdgpu_device_initialize(fd, &maj_ver, &min_ver, &dev);
	if (ret) {
		fprintf(stderr, "Failed to create DRM device\n");
		return 1;
	}

	ret = amdgpu_cs_ctx_create(dev, &ctx);
	if (ret) {
		fprintf(stderr, "Failed to create GPU context\n");
		return 1;
	}

	Buffer cmdbuffer;
	ret = setup_cmdbuffer(&cmdbuffer);
	if (ret) {
		fprintf(stderr, "Failed to create cmdbuffer\n");
		return 1;
	}

	fprintf(stdout, "start allocating buffers\n");

	std::vector<Buffer> buffers(buffer_count);
	for (unsigned i = 0; i < buffer_count; ++i) {
		ret = create_buffer(1024*1024*1024, 2 * 1024 * 1024, domain, flags, &buffers[i]);
		if (ret) { 
			fprintf(stderr, "Failed to create buffer %d\n", i);
			return 1;
		}
	}

	fprintf(stdout, "finish allocating buffers\n");

	for (unsigned i = 0; i < buffer_count; ++i) {
		fprintf(stdout, "starting run %d\n", i);
		if(run_test(&cmdbuffer, &buffers[i])) {
			fprintf(stderr, "failed run %d\n", i);
			return 1;
		}
	}
	fprintf(stdout, "finished\n");
	return 0;
}
